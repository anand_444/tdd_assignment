package com.anandvardhan;

import java.util.ArrayList;
import java.util.List;

public class PowerCalculator {
    public int x,y,z,result;
    public List<Integer> list = new ArrayList<>();
    public int  ModularExponentiation(int x,int y,int z){

            result = 1;
            x = x % z;
            while (y >= 1) {

                if ((y & 1) == 1) {
                    result = result * x;
                }
                x = x * x;
                y >>= 1;
                x = x % z;
                result = result % z;
            }

        return result;
    }
    public static void main(String[] args) {
        MyFileRead fileData = new MyFileRead();
        List <Integer> list =fileData.getFromFile("in.txt");
        PowerCalculator power =new PowerCalculator();
        int x,y,z;
        for (int i=0;i<list.size();i=i+3) {
            x = list.get(i);
            y = list.get(i + 1);
            z = list.get(i + 2);
            System.out.println("x:" + x + ", y:" + y + ", m:" + z + " (power(x,y) mod m) is " + power.ModularExponentiation(x,y,z));
        }
    }
}
