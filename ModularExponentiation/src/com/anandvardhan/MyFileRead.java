package com.anandvardhan;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class MyFileRead {

    public List<Integer> getFromFile(String filename) {
        List<Integer> fileData =new ArrayList<>();

        Scanner scanner;
        try {
            scanner = new Scanner(new File(filename));
            int x, y, z, t;
            t = scanner.nextInt();
            scanner.nextLine();
            while (t-- > 0) {
                x = scanner.nextInt();
                y = scanner.nextInt();
                z = scanner.nextInt();
                scanner.nextLine();
                fileData.add(x);
                fileData.add(y);
                fileData.add(z);
                //Power object1 = new Power();
                //return list;
                //System.out.println("x:" + x + ", y:" + y + ", m:" + m + " (power(x,y) mod m) is " + object1.ModularExponentiation(x, y, m));
            }
        } catch (
                FileNotFoundException e) {
            e.printStackTrace();
        }
        System.out.println(fileData);
        return fileData;
    }


}
