package com.anandvardhan;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class PowerCalculatorTest {

    int x,y,z;

    @Before
    public void setUp(){
        x = 2;
        y = 3;
        z = 5;
    }

    @Test
    public void testCanGetACorrectAnswer() {

        PowerCalculator power = new PowerCalculator();
        int answer = power.ModularExponentiation(x, y, z);

        assertEquals(3, answer);
    }
    @Mock
    MyFileRead mockFileObj;

    @Test
    public void testMock() {

        MockitoAnnotations.initMocks(this);
        when(mockFileObj.getFromFile(anyString())).thenReturn(getMocKFile());
       // List<Integer> list =new ArrayList<>();
        //list= mockObj.getFromFile("in.txt");
        assertEquals(Integer.valueOf(22),mockFileObj.getFromFile("in.txt").get(0));
        verify(mockFileObj).getFromFile("in.txt");

    }

    public List<Integer> getMocKFile(){
        List <Integer> mockList=new ArrayList<>();
        mockList.add(22);
        return mockList;
    }
}


